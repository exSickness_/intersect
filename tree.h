
#ifndef TREE_H
	#define TREE_H

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>


#include "structs.h"
#include "avl.h"


bst* bst_create(int (*cmp)(const struct node* , const struct node* ));
bool bst_insert(bst* bst, struct node* stock);
char* bst_fetch(bst* b, const char* key);
void print_bst(bst* bst);
void bst_destroy(bst* bst, void (*dismantle)(struct tree* t));


void print_tree(struct tree* t);
void tree_destroy(struct tree* t);
void tree_disassemble(struct tree* t);

int cmp(const struct node* , const struct node* );

#endif
