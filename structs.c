
#include "structs.h"


struct node* node_create(char* word)
{
	struct node* new_node = malloc( sizeof(*new_node) );
	if (!new_node) {
		return(NULL);
	}

	new_node->word = strdup(word);
	return(new_node);
}

void node_destroy(struct node* n)
{
	if (n) {
		free(n->word);
		free(n);
	}
	return;
}