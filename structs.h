#ifndef ISTRUCTS_H
	#define ISTRUCTS_H

#include <stdlib.h>
#include <string.h>

struct node {
	char* word;
};

struct tree {
	struct node* data;
	struct tree* left,* right;
	int height;
};

typedef struct {
	struct tree* root;
	int (*cmp)(const struct node* , const struct node* );
} bst;

typedef struct{
	size_t capacity;
	size_t size;
	bst** data;
} hash;


struct node* node_create(char* word);
void node_destroy(struct node* n);


#endif