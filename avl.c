
#include "avl.h"

#include "structs.h"

int height(struct tree* t)
{
    if (!t) {
        return(0);
	}
    return(t->height);
}

int get_balance(struct tree* t)
{
    if (!t) {
        return(0);
	}
    return( height(t->right) - height(t->left) );
}

struct tree* rot_right(struct tree* t)
{
	if (!t) {
		return(NULL);
	}

    struct tree* old_left = t->left;
    struct tree* tmp = old_left->right;

    // Rotating pointers
    old_left->right = t;
    t->left = tmp;

    // Update height
	t->height = (height(t->left) > height(t->right) ? height(t->left) : height(t->right)) + 1;
	old_left->height = (height(old_left->left) > height(old_left->right) ? height(old_left->left) : height(old_left->right)) + 1;

	// Update root
    return(old_left);
}

struct tree* rot_left(struct tree* t)
{
 	if (!t) {
		return(NULL);
	}

	struct tree* old_right = t->right;
    struct tree* tmp = old_right->left;

    // Rotating pointers
    old_right->left = t;
    t->right = tmp;

    // Update height
	t->height = (height(t->left) > height(t->right) ? height(t->left) : height(t->right)) + 1;
	old_right->height = (height(old_right->left) > height(old_right->right) ? height(old_right->left) : height(old_right->right)) + 1;

	// Update root
    return(old_right);
}