#ifndef INTERSECT_H
	#define INTERSECT_H


hash* parseInitialFile(char* fileName, hash* hashOrig);
hash* parseSubsequentFiles(int argc, char* argv[],hash* hashOrig);


#endif