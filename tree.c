
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include "tree.h"


bst* bst_create(int (*cmp)(const struct node* , const struct node* ))
{
	bst* head = malloc( sizeof(*head) );

	if(head)
	{
		head->root = NULL;
		head->cmp = cmp;
	}

	return(head);
}

static struct tree* tree_create(struct node* node)
{
	struct tree* new_tree = malloc( sizeof(*new_tree) );

	if (new_tree)
	{
		new_tree->data = node;
		new_tree->left = NULL;
		new_tree->right = NULL;
		new_tree->height = 1;//initial height
	}

	return(new_tree);
}

static struct tree* tree_insert(struct tree* t, struct node* node, int (*cmp)(const struct node* , const struct node* ))
{
	if (!t) {
		return( tree_create(node) );
	}

	if (strcasecmp(node->word, t->data->word) == 0) // found same word
	{
		node_destroy(node);
		return(t);
	}

	if (cmp(node, t->data) < 0) {
		t->left = tree_insert(t->left, node, cmp);
	} else {
		t->right = tree_insert(t->right, node, cmp);
	}

	// AVL Arrangement - Worked on with SGT McLaurin
	t->height = ((height(t->left) > height(t->right)) ? height(t->left) : height(t->right)) + 1;

	int balance = get_balance(t);
	int balance_child = 0;

	if (balance > 0) {
		balance_child = get_balance(t->right);
	}
	else {
		balance_child = get_balance(t->left);
	}

	// Left-Left (Parent left)
	if (balance > 1 && balance_child >= 0) {
		return( rot_left(t) );
	}

	// Right-Right(Parent right)
	if (balance < -1 && balance_child < 0) {
		return( rot_right(t) );
	}

	// Left-Right(Parent left, Child right)
	if (balance > 1 && balance_child < 0)
	{
		t->right = rot_right(t->right);
		return( rot_left(t) );
	}

	// Right-Left(Parent right, Child left)
	if (balance < -1 && balance_child >= 0)
	{
		t->left = rot_left(t->left);
		return( rot_right(t) );
	}
	return(t);
}

bool bst_insert(bst* bst, struct node* n)
{
	if (!bst) {
		return(false);
	}
	else if (!bst->root)
	{
		// No root. Make node the root
		bst->root = tree_create(n);
		return(true);
	}
	bst->root = tree_insert(bst->root, n, bst->cmp);
	return(true);
}



void tree_destroy(struct tree* t)
{
	if (!t) {
		return;
	}

	tree_destroy(t->left);
	tree_destroy(t->right);

	node_destroy(t->data);
	free(t);
}

void tree_disassemble(struct tree* t)
{
	if (!t) {
		return;
	}

	tree_disassemble(t->left);
	tree_disassemble(t->right);

	free(t->data->word);
	free(t->data);
	free(t);
}

void bst_destroy(bst* bst, void (*dismantle)(struct tree* t))
{
	if (!bst || !bst->root) {
		return;
	}

	(*dismantle)(bst->root);
	free(bst);
}

void print_tree(struct tree* t)
{
	if (!t) {
		return;
	}
	// static int indent = 0;
	// indent +=1;
	print_tree(t->left);
	// int n = indent;
	// while(--n) {
		// printf(" ");
	// }
	printf("%s\n", t->data->word);
	print_tree(t->right);
	// indent -=1;
}

void print_bst(bst* bst)
{
	if (!bst || !bst->root) {
		return;
	}

	print_tree(bst->root);
}

int cmp(const struct node* node1, const struct node* node2)
{
	if (!node1 || !node2) {
		return(0);
	}

	if( strcasecmp(node1->word, node2->word) <= 0 ) {
		return(-1);
	} else {
		return(1);
	}
}


static char* tree_search(struct tree* t, const char* key)
{
	if(!t) {
		return(0);
	}

	int retVal = strcasecmp(key,t->data->word);

	if( retVal == 0) { // Found the key
		return(t->data->word);
	}
	else if(retVal > 0) {
		return( tree_search(t->right, key) );
	}
	else {
		return( tree_search(t->left, key) );
	}
}

char* bst_fetch(bst* b,const char* key)
{
	if(!b) {
		return(NULL);
	}

	return( tree_search(b->root, key) );
}