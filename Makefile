CFLAGS+=-std=c11 -Wall -Werror -Wno-deprecated -Wextra -Wstack-usage=1024 -pedantic -fstack-usage -D _XOPEN_SOURCE=800 -D _BSD_SOURCE


intersect: intersect.o hash.o tree.o structs.o avl.o


.PHONY: clean debug profile

clean:
	rm intersect *.o *.su

debug: CFLAGS+=-g
debug: intersect

profile: CFLAGS+=-pg
profile: LDFLAGS+=-pg
profile: intersect