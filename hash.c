#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hash.h"

static const size_t DEFAULT_SIZE = 128;

static size_t hashish(const char* key);


hash* __hash_create(size_t capacity)
{
	hash* h = malloc(sizeof(*h));
	if(!h) {
		return(NULL);
	}

	h->data = calloc(capacity, sizeof(*h->data) );
	if(!h->data) {
		free(h);
		return(NULL);
	}

	h->capacity = capacity;
	h->size = 0;

	return(h);
}

hash* hash_create(void)
{
	return(__hash_create(DEFAULT_SIZE) );
}

void hash_destroy(hash* h)
{
	if(h) {
		for(size_t n = 0; n < h->capacity; n++) {
			// Destroy each BST at every hash index
			bst_destroy(h->data[n], tree_destroy);
		}

		free(h->data);
		free(h);
	}
}

void hash_disassemble(hash* h)
{
	if(h) {
		for(size_t n = 0; n < h->capacity; n++) {
			// Disassemble each BST at every hash index
			bst_destroy(h->data[n], tree_disassemble);
		}

		free(h->data);
		free(h);
	}
}

static size_t hashish(const char* key)
{
	if( key[0] >= 'A' && key[0] <= 'Z' )
	{
		char tempChar[3];
		strncpy( tempChar,key,1 );
		return( tolower( tempChar[0] ) );
	}
	return( key[0] );

}


bool hash_insert(hash* h,char* key)
{
	if(!h || !key) {
		return(false);
	}
	size_t idx = hashish(key);

	struct node* tmp_node = node_create(key);

	// Check for BST at hash index
	if(!h->data[idx])
	{
		// No BST. Creating BST at hash index & insert
		h->data[idx] = bst_create(cmp);
		bst_insert(h->data[idx],tmp_node);
	}
	else {
		// BST found. Insert node
		bst_insert(h->data[idx],tmp_node);
	}
	h->size += 1;

	return(true);
}



char* hash_fetch(hash* h, const char* key)
{
	if(!h || !key) {
		return(NULL);
	}

	size_t idx = hashish(key);

	// Search BST at hash index for key
	return( bst_fetch(h->data[idx],key) );
}

void hash_print(hash* h)
{
	for(size_t n = 0; n < h->capacity; ++n)
	{
		if(h->data[n])
		{
			print_bst(h->data[n]);
		}
		else {
			/* Do nothing */
		}
	}
}