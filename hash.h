#ifndef HASH_H
	#define HASH_H

#include <stdbool.h>

#include "tree.h"

hash* hash_create(void);
void hash_destroy(hash* h);
void hash_disassemble(hash* h);

bool hash_insert(hash* h, char* key);
char* hash_fetch(hash* h, const char* key);

void hash_print(hash* h);

#endif
