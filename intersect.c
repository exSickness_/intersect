#include <ctype.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "hash.h"
#include "tree.h"
#include "intersect.h"
#include "structs.h"

/* References
 * http://sethrobertson.github.io/GitFixUm/fixup.html
*/

int main(int argc, char* argv[])
{
	if (! argc >= 2) {
		fprintf(stderr,"\n\tInvalid number of arguments.\n");
		return(1);
	}

	hash* hashOrig = hash_create();

	hashOrig = parseInitialFile(argv[1], hashOrig);
	if(!hashOrig) {
		fprintf(stderr,"\n\tError parsing initial file.\n");
		return(1);
	}

	hashOrig = parseSubsequentFiles(argc, argv, hashOrig);
	if(!hashOrig) {
		fprintf(stderr,"\n\tError parsing subsequent file.\n");
		return(1);
	}

	hash_print(hashOrig);
	hash_destroy(hashOrig);
	return(0);
}


 hash* parseInitialFile(char* fileName, hash* hashOrig)
 {
	size_t sz = 256;
	char* buf = malloc(sz);
	FILE* fp = NULL;

 	fp = fopen(fileName, "r+");
	if(!fp) {
		fprintf(stderr,"\n\tError opening file.\n");
		return(NULL);
	}
	// Loop for reading entire file
	while(fscanf(fp,"%s",buf) == 1)
	{
		if( isascii(buf[0]) ) {
			if( !hash_fetch(hashOrig,buf) )
			{
				// Insert if the word was not found
				hash_insert(hashOrig,buf);
			}
			else {
				/* Do nothing */
			}
		}
	}
	free(buf);
	fclose(fp);

 	return(hashOrig);
 }

 hash* parseSubsequentFiles(int argc, char* argv[],hash* hashOrig)
 {
 	size_t sz = 256;
	char* buf = malloc(sz);
	FILE* fp = NULL;
	char* tempWord = NULL;

	// Loop for each file
 	for(size_t i = 2; i < (unsigned)argc ; i++)
	{
		hash* hashDup = hash_create();

		fp = fopen(argv[i], "r+");
		if(!fp) {
			fprintf(stderr,"\n\tError opening file: %s\n",argv[i]);
			return(NULL);
		}
		// Loop for reading file
		while(fscanf(fp,"%s",buf) == 1)
		{
			if( isascii(buf[0]) ) {
				if( !(tempWord = hash_fetch(hashOrig,buf)) )
				{
					/* Do nothing */
				}
				else {
					// Insert duplicate word into second hash
					hash_insert(hashDup,tempWord);
				}
			}
		}
		// Point orifinal hash to duplicate and disassemble second hash
		hash* tempHash = hashOrig;
		hashOrig = hashDup;
		hash_disassemble(tempHash);

		fclose(fp);
	}
	free(buf);
	return(hashOrig);
 }
