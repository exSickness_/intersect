#ifndef IAVL_H
	#define IAVL_H

#include "structs.h"

int height(struct tree* t);
int get_balance(struct tree* t);

struct tree* rot_left(struct tree* t);
struct tree* rot_right(struct tree* t);

#endif